import numpy as np

dtype_mapping = {
    '<f4': 'float64'
}

def upsample(array):
    dtype = array.dtype
    new_dtype = [(key, dtype_mapping.get(value, value)) for key, value in dtype.descr]
    return array.astype(new_dtype)

def reshape(array):
    return array.reshape(len(array))

def read(array):
    return upsample(reshape(array))

class TAQReader:
    def __init__(self, hdf5):
        self.hdf5 = hdf5
    def dates(self):
        return self.hdf5.keys()
    def tickers(self, date):
        if date in self.dates():
            return self.hdf5[date].keys()
        else:
            return []
    def prices(self, date, ticker):
        raise NotImplementedError
    def open_close(self, date, ticker):
        raise NotImplementedError
    def all(self, date, ticker):
        return self.hdf5[date][ticker][...]
    def missing(self, date, ticker):
        if not date in self.hdf5:
            return True
        elif not ticker in self.hdf5[date]:
            return True
        else:
            return False

class TradeReader (TAQReader):
    def prices(self, date, ticker):
        return upsample(self.hdf5[date][ticker]['m', 'p'])
    def open_close(self, date, ticker):
        dataset = self.hdf5[date][ticker]
        if (len(dataset) == 1):
            first_row = dataset['p', [0]]
            return np.concatenate([first_row, first_row])
        else:
            return dataset['p', [-1,0]]
    def volume(self, date, ticker):
        return self.hdf5[date][ticker]['s'].sum()

class QuotesReader (TAQReader):
    def prices(self, date, ticker):
        bid_ask = upsample(self.hdf5[date][ticker]['m', 'bp', 'ap'])
        mid = (bid_ask['bp'] + bid_ask['ap'])/2
        prices = np.rec.fromarrays([bid_ask['m'], mid])
        prices.dtype.names = ('m', 'p')
        return prices
    def open_close(self, date, ticker):
        dataset = self.hdf5[date][ticker]
        if (len(dataset) == 1):
            print(ticker)
            first_row = upsample(dataset[[0]][['ap', 'bp']])
            bid_ask = np.hstack([first_row, first_row])
        else:
            bid_ask = upsample(dataset[[-1, 0]][['ap', 'bp']])
        return (bid_ask['ap'] + bid_ask['bp'])/2

