import statsmodels.tsa.stattools as tsa

def multiindex_to_dict(df, level):
    return {index: df.xs(index, level=level) for index in df.index.levels[level]}

def test_autocorrelation(bucketed_taq):
    bucket_counts = np.sort(np.array(bucketed_taq, dtype=np.int))
    def hypothesis_tester(ticker, bucket_count):
        # returns whether it is possible to reject the null-hypothesis
        # null-hypothesis = there is no autocorrelation
        corr, ci, _, p = tsa.acf(bucketed_taq[str(bucket_count)][ticker][...], unbiased=True, nlags=10, fft=True, alpha=0.05, qstat=True)
        return pd.Series((not (ci[1][0] < 0 < ci[1][1]), p[0] < 0.05), index=['single', 'ljung-box'])
    df = pd.concat({bucket_count: pd.DataFrame({ticker: hypothesis_tester(ticker, bucket_count) for ticker in tickers}) for bucket_count in bucket_counts})
    return multiindex_to_dict(df, 1)


from collections import OrderedDict
from scipy.stats import chi2

def returns_stats(df):
    functions = ([
        ('mean', pd.DataFrame.mean),
        ('standard deviation', pd.DataFrame.std),
        ('skew', pd.DataFrame.skew),
        ('kurtosis', pd.DataFrame.kurtosis),
        ('count', pd.DataFrame.count)
    ])
    return pd.DataFrame(OrderedDict([(function_name, function(df)) for function_name, function in functions]))

def jarque_bera(skew, kurtosis, sample_size):
    gamma = skew
    kappa = kurtosis
    N = sample_size
    return chi2.cdf((gamma**2+((kappa-3)/4)**2)*N/6, df=2)

def question_3a(bucket_count, bucketed_taq):
    df = pd.DataFrame({ticker: bucketed_taq[str(bucket_count)][ticker][...] for ticker in tickers})
    statistics = returns_stats(df)
    p_values = statistics[['skew', 'kurtosis', 'count']].apply(lambda row: jarque_bera(*row), axis=1)
    statistics['reject normality'] = p_values.apply(lambda x: x > 0.95)
    statistics.drop('count', axis=1, inplace=True)
    return statistics
    
def question_3b(bucketed_group):
    p_values = pd.Series([tsa.adfuller(bucketed_group[ticker])[0] for ticker in tickers], index=tickers)
    reject = p_values.apply(lambda p: p < 0.05)
    return reject

from ggplot import aes, ggplot, geom_bar, labs, ggsave

def correlation_by_bucket_length(ticker, bucketed_taq):
    bucket_counts = np.sort(np.array(bucketed_taq, dtype=np.int))
    def correlation(bucket_count):
        return tsa.acf(bucketed_taq[str(bucket_count)][ticker][...], unbiased=True, nlags=1, fft=True)[1]

    y = np.vectorize(correlation)(bucket_counts)
    x = day_length/bucket_counts
    return ggplot(aes(x='x', y='y'), data={'x':[str(z) for z in x], 'y':y}) \
    + geom_bar(stat='identity') \
    + labs(x='bucket length', y='correlation')