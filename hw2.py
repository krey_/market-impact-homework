from tickers import MostLiquidTickerList
from utils import StoredValue, dataframe_from_function, np_from_function
from default_taq_read import readers
import numpy as np

stored = {}

def find_most_liquid_tickers():
    return MostLiquidTickerList(readers['trade'], 1500).list()

stored['mostliquid'] = StoredValue('./pickle/mostliquid.pkl', find_most_liquid_tickers)

def make_hw2_validity_df():
    dates = list(readers['trade'].dates())

    def valid_condition(date, ticker):
        min_quotes = 5
        quotes_reader = readers['quotes']
        if quotes_reader.missing(date, ticker):
            return False
        else:
            dataset = quotes_reader.hdf5[date][ticker]
            return (dataset.shape[0] >= min_quotes)

    return dataframe_from_function(valid_condition, dates, stored['mostliquid'].get())

stored['hw2_validity'] = StoredValue('./pickle/valid.pkl', make_hw2_validity_df)


### (date, ticker) functions ###

import buckets
from buckets import bucketed_returns, bucket_count_from_length
import pandas as pd

def returns_2min (date, ticker):
    return bucketed_returns(date, ticker, bucket_count_from_length(2*60), readers['quotes'])

def total_daily_volume(date, ticker):
    return readers['trade'].volume(date, ticker)

def arrival_price(date, ticker):
    return readers['quotes'].prices(date, ticker)[:5]['p'].mean()

def terminal_price(date, ticker):
    return readers['quotes'].prices(date, ticker)[-5:]['p'].mean()

def vwap_at_330(date, ticker):
    trades = pd.DataFrame.from_records(readers['trade'].all(date, ticker))
    end_time = buckets.close_time - 30*60*1000
    truncated_trades = trades[trades['m'] <= end_time]
    volume = truncated_trades['s'].sum()
    if volume == 0:
        return np.nan
    else:
        return (truncated_trades['s'] * truncated_trades['p']).sum()/volume

def vwap(date, ticker):
    trades = pd.DataFrame.from_records(readers['trade'].all(date, ticker))
    volume = trades['s'].sum()
    if volume == 0:
        return np.nan
    else:
        return (trades['s'] * trades['p']).sum()/volume

def imbalance(date, ticker):
    trades = pd.DataFrame(readers['trade'].all(date, ticker)[['p', 's']])
    signs = np.sign(trades['p']-trades['p'].shift(1))
    signs[signs == 0] = np.nan
    tick_test = signs.fillna(method='ffill')
    return (trades['s'] * trades['p'] * tick_test).sum()

### and functions to store them ###

from utils import Timer

dateticker_functions = {
    'returns_2min': returns_2min,
    'total_daily_volume': total_daily_volume,
    'arrival_price': arrival_price,
    'vwap_at_330': vwap_at_330,
    'vwap': vwap,
    'imbalance': imbalance,
    'terminal_price': terminal_price
}

from nd_convert import ndarray_from_vector_function, h5py_string_axis, ndarray_to_hdf5

def store_dateticker_function(function, dates, tickers, file_path, dataset_path, overwrite=False):
    values, axes = ndarray_from_vector_function(function, [h5py_string_axis(dates), h5py_string_axis(tickers)], upcast=True)
    ndarray_to_hdf5(values, axes, file_path, dataset_path, overwrite)
    
def store_dateticker_functions(dates, tickers, file_path, overwrite=False):
    for name, function in dateticker_functions.items():
        print(name)
        with Timer():
            store_dateticker_function(function, dates, tickers, hdf5, name, overwrite)
    
def isnan_2D(x):
    '''Looks for NaNs in a multidimensional array, returning a 2 dimensional one.
    It treats the input x as [[array]] and then maps
    a function isnan: array -> Bool onto it
    returning [[Bool]]
    '''
    return np.apply_over_axes(np.any, np.isnan(x), [axis for axis in range(len(x.shape)) if axis >= 2]).reshape([x.shape[0], x.shape[1]])

def lookback_volatility(returns_df, lookback_window):
    buckets = returns_df.index.levshape[1]
    rolling_std = pd.rolling_std(returns_df, lookback_window*buckets).shift(1)
    # now pick first value for each day and throw away the NaN days
    return rolling_std.xs(0, level=1).iloc[lookback_window:,:] * np.sqrt(buckets)

def lookback_mean(dateticker_df, lookback_window):
    return pd.rolling_mean(dateticker_df, lookback_window).shift(1).iloc[lookback_window:, :]