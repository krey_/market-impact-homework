import h5py
import numpy as np
import scipy.io as sio
import re
import os.path

import sys
sys.path.append('.')
from utils import ExtractedTarball

class Filename:
    pattern = re.compile(r"([0-9]{8})/([A-Z]{1,4})_(trades|quotes).mat")
    @classmethod
    def parse(cls, name):
        matches = cls.pattern.match(name)
        if not matches is None:
            date, ticker, kind = matches.groups()
            return {'date': date, 'ticker': ticker, 'kind': kind}
        else:
            return None
    @staticmethod
    def format(date, ticker, kind):
        return date+'/'+ticker+'_'+kind+'.mat'

def list_dir_relative(directory):
    return [os.path.relpath(os.path.join(dp, f), directory) for dp, _, fs in os.walk(directory) for f in fs]

def load_tickers(date, directory):
    files = list_dir_relative(directory)
    parsed_names = (Filename.parse(file)for file in files)
    return [parsed['ticker'] for parsed in parsed_names if not parsed is None]

def load_dates(directory):
    files = list_dir_relative(directory)
    def parse_fname(fname):
        matches = re.match(r'([0-9]{8}).tar.gz', fname)
        if matches is None:
            return None
        else:
            return matches.group(1)
    return sorted([file for file in map(parse_fname, files) if file is not None])

#######
# TAQ #
#######

def taq_to_numpy(matlab_data, dtype):
    columns = [matlab_data[col][0][0].transpose()[0] for col, _ in dtype]
    return np.rec.fromarrays(columns, np.dtype(dtype))

dtypes = {
    'trade': [('m', 'int32'), ('s', 'int32'), ('p', 'float32')],
    'quotes': [('m', 'int32'), ('bp', 'float32'), ('bs','int32'), ('ap', 'float32'), ('as', 'int32')]
}

def load_taq_from_first_format(date, ticker, hdf5):
    dataset = hdf5[date][ticker]
    return {'e': dataset.attrs['e'],
            'data': dataset[...].transpose()[0],
            'ticker': ticker,
            'date': date
           }

def load_taq(date, ticker, directory, kind):
    path = os.path.join(directory, Filename.format(date, ticker, kind))
    matlab_data = sio.loadmat(path)['data']
    np_data = taq_to_numpy(matlab_data, dtypes[kind])
    e = matlab_data['e'][0][0][0][0]
    return {'e': e, 'data': np_data, 'ticker': ticker, 'date': date}

def save_taq(taq, hdf5, overwrite = False):
    path = taq['date']+'/'+taq['ticker']
    if path in hdf5 and overwrite:
        del hdf5[path]
    if not path in hdf5:
        #print('Creating dataset')
        hdf5.create_dataset(path,
                              shape=taq['data'].shape,
                              dtype=taq['data'].dtype,
                              compression='lzf',
                              shuffle=True)
        dataset = hdf5[path]
        dataset.attrs['e'] = taq['e']
        dataset[...] = taq['data']
    else:
        #print('Doing nothing')
        pass

def load_and_save_taq_on_date(date, directory, hdf5_path, kind, temp_dir = None):
    with ExtractedTarball(os.path.join(directory, date + ".tar.gz"), temp_dir) as dir:
        tickers = load_tickers(date, dir)
        with h5py.File(hdf5_path) as trades:
            for ticker in tickers:
                #print(date + '/' + ticker)
                save_taq(load_taq(date, ticker, dir, kind),trades)

