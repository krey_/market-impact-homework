import h5py
import pandas as pd
import numpy as np

def ndarray_to_hdf5(values, axes, file_path, dataset_path, overwrite=False):
    with h5py.File(file_path, libver='latest') as hdf5:

        if dataset_path in hdf5:
            if overwrite:
                del hdf5[dataset_path]
            else:
                return
        
        assert tuple(map(len, axes)) == values.shape

        hdf5.create_dataset(dataset_path,
                     shape=values.shape,
                     dtype=values.dtype,
                     compression='lzf',
                     shuffle=True
                    )
        dataset = hdf5[dataset_path]
        for i, axis in enumerate(axes):
            dataset.attrs['ax'+str(i)] = axis
        dataset[...] = values
    
def h5py_string_axis(axis):
    '''Stop h5py complaining about strings'''
    return np.array(axis, dtype=h5py.special_dtype(vlen=str))

def hdf5_to_ndarray(file_path, dataset_path):
    with h5py.File(file_path, 'r', libver='latest') as hdf5:
        dataset = hdf5[dataset_path]
        axis_count = len(dataset.shape)
        axes = [dataset.attrs['ax'+str(i)] for i in range(axis_count)]
        values = dataset[...]
        return (values, axes)

def ndarray_to_pd(values, axes, row_axes=[0], col_axes=[1]):
    assert set(row_axes) | set(col_axes) == set(range(len(values.shape)))
    lens = np.array(list(map(len, axes)))
    axes = np.array(axes)
    
    index = pd.MultiIndex.from_product(axes[row_axes])
    columns = pd.MultiIndex.from_product(axes[col_axes])
    data = values.transpose(row_axes + col_axes).reshape(np.prod(lens[row_axes]), np.prod(lens[col_axes]))
    
    return pd.DataFrame(data, index=index, columns=columns)

def pd_to_ndarray(df):
    def labels(axis):
        return getattr(axis, 'levels', [axis])

    axes = labels(df.index) + labels(df.columns)
    lens = np.array(list(map(len, axes)))

    return (df.values.reshape(lens), axes)

import itertools

def ndarray_from_function(func, axes, upcast=False):
    '''Doesn't work with functions that return more than a scalar
    upcast should be set to True on weird return type behaviour
    '''
    dtype = object if upcast else None
    cartesian_product = np.array(list(itertools.product(*axes)), dtype=dtype)
    meshgrid = cartesian_product.reshape(*map(len, axes), len(axes)).transpose(len(axes),*range(len(axes)))
    return np.vectorize(func)(*meshgrid)

def ndarray_from_vector_function(func, axes, upcast=False):
    '''upcast should be set to True on weird return type behaviour'''
    dtype = object if upcast else None
    cartesian_product = np.array(list(itertools.product(*axes)), dtype=dtype)
    hierarchical_cartesian_product = cartesian_product.reshape(*map(len, axes), len(axes))
    values = np.apply_along_axis(lambda x: func(*x), -1, hierarchical_cartesian_product)
    return (values, add_missing_axes(values, axes))

def old_hdf5_to_ndarray(file_path, dataset_path):
    with h5py.File(file_path, 'r', libver='latest') as hdf5:
        dataset = hdf5[dataset_path]
        axis_count = len(dataset.shape)
        axes = [dataset.attrs['dates'], dataset.attrs['tickers']] + [np.array(range(dataset.shape[i+2])) for i in range(axis_count-2)]
        values = dataset[...]
        return (values, axes)
    
def add_missing_axes(values, axes):
    missing = len(values.shape) - len(axes)
    # assert missing >= 0
    new_axes = [np.array(list(range(dim)))for dim in values.shape[len(axes):]]
    return axes + new_axes