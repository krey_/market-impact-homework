import numpy as np

open_time = int(9.5 * 3600) * 1000
close_time = int(16 * 3600) * 1000
minutes_per_day = int((close_time - open_time) / 1000)

def bucket_count_from_length(period_length):
    '''Input the length of the period, output the number of buckets'''
    return int(minutes_per_day/period_length)

def bucket(times, values, initial_value, bucket_times):
    # times, values are a time series
    bucket = 0
    time = 0
    bucket_count = len(bucket_times)
    time_count = len(times)

    bucketed = np.empty(bucket_count) * np.nan

    while bucket < bucket_count and time < time_count:
        if times[time] <= bucket_times[bucket]:
            time += 1
        else:
            bucketed[bucket] = initial_value if time == 0 else values[time-1]
            bucket += 1
    
    bucketed[bucket:] = values[-1]
    return bucketed

def np_bucket(times, values, initial_value, bucket_times):
    after_bucket_times = np.searchsorted(times, bucket_times, side='right')
    return np.hstack((initial_value, values))[after_bucket_times]

def bucketed_returns(date, ticker, bucket_count, taq_reader):
    prices = taq_reader.prices(date, ticker)
    bucket_times = np.linspace(open_time, close_time, bucket_count+1)
    bucketed = np_bucket(prices['m'], prices['p'], prices['p'][0], bucket_times)
    return np.diff(np.log(bucketed))

def bucketed_returns_per_day(date, bucket_count, taq_reader):
    tickers = tickers_per_day(date, taq_reader) # no longer works
    return np.vstack([taq_reader.bucketed_returns(date, ticker, bucket_count) for ticker in tickers])

def bucketed_returns_per_ticker(ticker, bucket_count, taq_reader):
    dates = taq_reader.dates()
    return np.concatenate([bucketed_returns(date, ticker, bucket_count, taq_reader) for date in dates])

def save_bucket_returns(ticker, bucket_count, taq_reader, hdf5, overwrite = True):
    path = str(bucket_count)+'/'+ticker
    if path in hdf5 and overwrite:
        del hdf5[path]
    bucketed = bucketed_returns_per_ticker(ticker, bucket_count, taq_reader)
    hdf5.create_dataset(path,
                         shape=bucketed.shape,
                         dtype=bucketed.dtype,
                         compression='lzf',
                         shuffle=True
                        )
    dataset = hdf5[path]
    dataset[...] = bucketed

def _save_bucket_returns_transposed(date, bucket_count, taq_reader, hdf5, overwrite = True):
    path = str(bucket_count)+'/'+date
    if path in hdf5 and overwrite:
        del hdf5[path]
    bucketed = bucketed_returns_per_day(date, bucket_count, taq_reader)
    hdf5.create_dataset(path,
                         shape=bucketed.shape,
                         dtype=bucketed.dtype,
                         compression='lzf',
                         shuffle=True
                        )
    dataset = hdf5[path]
    dataset.attrs['tickers'] = np.array(list(tickers_per_day(date, taq_reader)), dtype='U5')
    dataset[...] = bucketed
    
def save_bucket_returnss(tickers, bucket_counts, taq_reader, hdf5, overwrite = True):
    for bucket_count in bucket_counts:
        for ticker in tickers:
            save_bucket_returns(ticker, bucket_count, taq_reader, hdf5, overwrite)