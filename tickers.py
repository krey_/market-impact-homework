import pickle

from itertools import islice
def nth(iterable, n, default=None):
    '''Returns the nth item or a default value'''
    return next(islice(iterable, n, None), default)

class TickerList:
    def list(self):
        '''Creates a list of tickers'''
        raise NotImplementedError
        
class EveryDayList (TickerList):
    '''Creates a list of tickers that are present every day'''
    def __init__(self, taq_reader, pre_filter):
        self.taq_reader = taq_reader
        self.pre_filter = pre_filter
    def list(self):
        date = nth(self.trade_reader.dates(), 0)
        tickers = set(filter(self._of_interest, taq_reader.tickers(date)))
        for date in taq_reader.dates():
            print(date)
            tickers.intersection_update(filter(self.of_interest, taq_reader.tickers(date)))
        return list(tickers)

class HW1TickerList (TickerList):
    '''Creates a list of tickers that start with A and are traded every day in the dataset'''
    def __init__(self, trade_reader, quotes_reader):
        self.trade_tickers= EveryDayList(trade_reader, self._of_interest)
        self.quotes_tickers = EveryDayList(quotes_reader, self._of_interest)
    def _of_interest(self, ticker):
        return ticker.startswith('A')
    def list(self):
        return list(set(self.trade_tickers.list()) & set(self.quotes_tickers.list()))

import pandas as pd

class MostLiquidTickerList (TickerList):
    '''Finds the top most traded names on the first day of the dataset'''
    def __init__(self, trade_reader, count):
        self.trade_reader = trade_reader
        self.count = count
    def list(self):
        trade_reader = self.trade_reader
        count = self.count
        date = nth(trade_reader.dates(), 0)
        tickers = list(trade_reader.tickers(date))
        volume_df = pd.DataFrame([len(trade_reader.prices(date, ticker)) for ticker in tickers], index=tickers)
        top_traded = volume_df.nlargest(count, columns=0).index
        return list(top_traded)