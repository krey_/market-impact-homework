import h5py
from taq_read import TradeReader, QuotesReader

paths = {
    'quotes': '/run/media/krey/Rum/TAQ/quotes_.hdf5',
    'trade': '/run/media/krey/Rum/TAQ/trade_.hdf5'
}

hdf5 = {
    'quotes': h5py.File(paths['quotes'], 'r', libver='latest'),
    'trade': h5py.File(paths['trade'], 'r', libver='latest')
}

readers = {
    'quotes': QuotesReader(hdf5['quotes']),
    'trade': TradeReader(hdf5['trade'])
}
