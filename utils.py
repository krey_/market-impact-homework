import time
class Timer:
    def __enter__(self):
        self.start = time.time()
    def __exit__(self, type, value, traceback):
        print("Took " + str(time.time()-self.start) + " secs")

import tempfile
import tarfile
import shutil
class ExtractedTarball:
    def __init__(self, path, temp_root = None):
        self.path = path
        self.temp_root = temp_root
    def __enter__(self):
        self.tempdir = tempfile.mkdtemp(dir=self.temp_root)
        with tarfile.open(self.path) as tar:
            tar.extractall(self.tempdir)
        return self.tempdir
    def __exit__(self, type, value, traceback):
        shutil.rmtree(self.tempdir, ignore_errors=True)


import pickle

class Pickler:
    def __init__(self, path):
        self.path = path

    def load(self):
        with open(self.path, 'rb') as f:
            return pickle.load(f)
    
    def save(self, thing):
        with open(self.path, 'wb') as f:
            pickle.dump(thing, f)

class StoredValue:
    def __init__(self, path, creator):
        self.pickler = Pickler(path)
        self.creator = creator
    def get(self):
        try:
            value = self.pickler.load()
        except FileNotFoundError:
            value = self.creator()
            pickler.save(value)
        return value

import numpy as np
import pandas as pd

def dataframe_from_function(func, rows, columns):
    return pd.DataFrame(np.frompyfunc(func, 2, 1).outer(rows, columns), index=rows, columns=columns)

def np_from_function(func, rows, columns):
    return np.stack([np.stack([func(row, column) for column in columns]) for  row in rows])